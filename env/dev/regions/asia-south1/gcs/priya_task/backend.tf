terraform {
  backend "gcs" {
    bucket = "priya-tfstate-backend-gcs"
    prefix = "terrafrom-improvement/env/dev/regions/asia-south-1/gcs/priya_task"
  }
}
